package com.vitoesposito;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Vector;

import com.sun.xml.internal.ws.util.StringUtils;

public class PhraseGenMain {

	public static void main(String[] args) throws IOException {
		File listDir = new File("." + File.separator + "lists");

		File[] lists = listDir.listFiles();
		for (File f : lists) {
			splitLists(f);
		}
	}

	private static void splitLists(File f) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(f));
		boolean requiresWriting = false;
		Vector<String> lines = new Vector<String>();
		{
			String line;
			while ((line = in.readLine()) != null) {
				lines.add(line);
			}
			in.close();
		}

		{
			Vector<String> linesMod = new Vector<String>();
			for (String line : lines) {
				String lineMod = toTitleCase(line);
				linesMod.add(lineMod);
				if (!lineMod.equals(line)) {
					requiresWriting = true;
				}
			}
			if (requiresWriting) {
				lines = linesMod;
				Collections.sort(lines);
			}
		}

		{
			Vector<String> linesMod = new Vector<String>();
			for (String line : lines) {
				String lineMod = trimComma(line);
				linesMod.add(lineMod);
				if (!lineMod.equals(line)) {
					requiresWriting = true;
				}
			}
			if (requiresWriting) {
				lines = linesMod;
				Collections.sort(lines);
			}
		}

		{
			Vector<String> linesMod = new Vector<String>();
			String lastLine = "";
			for (String line : lines) {
				if (lastLine.equals(line)) {
					requiresWriting = true;
				} else {
					linesMod.add(line);
				}
				lastLine = line;
			}
			if (requiresWriting) {
				lines = linesMod;
				Collections.sort(lines);
			}
		}

		for (String line : lines) {
			System.out.println(line);
		}

	}

	private static String trimComma(String line) {
		while (line.endsWith(",")) {
			line = line.substring(0, line.lastIndexOf(","));
		}
		line = line.trim();
		return line;
	}

	public static String toTitleCase(String input) {
		StringBuilder titleCase = new StringBuilder();
		boolean nextTitleCase = true;

		for (char c : input.toCharArray()) {
			if (Character.isSpaceChar(c)) {
				nextTitleCase = true;
			} else if (nextTitleCase) {
				c = Character.toTitleCase(c);
				nextTitleCase = false;
			}

			titleCase.append(c);
		}

		return titleCase.toString();
	}
}
